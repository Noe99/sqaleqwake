from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtWidgets import QInputDialog, QApplication, QWidget, QFileDialog

DATA = []

##Permet de lire un fichier text
def read(src):
    data = []
    fichier = open(src,"r")
    for ligne in fichier:
        # reading each word
        for word in ligne.split():
            # displaying the words
            # print(float(word))
            data.append(float(word))
    fichier.close()
    return data


