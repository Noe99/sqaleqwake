import csv
import io
import math

import pyqtgraph as pg
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtWidgets import *

from readFile import dialogCheck
from readFile import document


class Ui_MainWindow(QtWidgets.QMainWindow):
    ## Acceleration data
    accelerationX = []
    accelerationY = []

    ## Speed data
    speedX = []
    speedY = []

    ## Mouvement data
    mouvementX = []
    mouvementY = []

    pen = pg.mkPen(color=(255, 0, 0), width=3)  ## color use to draw

    mouseX = 0

    def setupUi(self, MainWindow):


        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1920, 1500)
        MainWindow.showMaximized()
        MainWindow.keyPressEvent = self.keyPressEvent


        screen = app.primaryScreen().size()
        maxTableSize = 0.2 * float(screen.width())
        self.dialogSize = screen.width()/3

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setAutoFillBackground(True)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")

        ## Acceleration graphic view
        self.graphicsView = pg.PlotWidget(self.centralwidget)
        self.graphicsView.setBackground('w')
        styles = {'color': 'w', 'font-size': '%spx' %(str(int(0.012 * screen.width())))}
        self.graphicsView.setLabel('left', 'Acceleration (m/s2)', **styles)
        self.graphicsView.setLabel('bottom', 'Time (s)', **styles)
        self.graphicsView.showGrid(x=True, y=True)
        self.graphicsView.setGeometry(QtCore.QRect(100, 20, 1700, 300))
        self.graphicsView.setObjectName("graphicsView")
        self.gridLayout.addWidget(self.graphicsView, 0, 2, 1, 1)

        self.graphicsView_2 = pg.PlotWidget(self.centralwidget)
        self.graphicsView_2.setBackground('w')
        self.graphicsView_2.setLabel('left', 'Velocity (m/s)', **styles)
        self.graphicsView_2.setLabel('bottom', 'Time (s)', **styles)
        self.graphicsView_2.showGrid(x=True, y=True)
        self.graphicsView_2.setGeometry(QtCore.QRect(100, 340, 1700, 300))
        self.graphicsView_2.setObjectName("graphicsView_2")
        self.gridLayout.addWidget(self.graphicsView_2, 2, 2, 1, 1)

        self.graphicsView_3 = pg.PlotWidget(self.centralwidget)
        self.graphicsView_3.setBackground('w')
        self.graphicsView_3.setLabel('left', 'Displacement (m/s)', **styles)
        self.graphicsView_3.setLabel('bottom', 'Time (s)', **styles)
        self.graphicsView_3.showGrid(x=True, y=True)
        self.graphicsView_3.setGeometry(QtCore.QRect(100, 660, 1700, 300))
        self.graphicsView_3.setObjectName("graphicsView_3")
        self.gridLayout.addWidget(self.graphicsView_3, 4, 2, 1, 1)

        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")

        ## PGA
        self.pgaLayout = QtWidgets.QHBoxLayout()
        self.pgaLayout.setObjectName("pgaLayout")

        self.label_1 = QLabel('',self)
        self.label_1.setMaximumSize(QtCore.QSize(10, 10))
        self.label_1.setMinimumSize(QtCore.QSize(10, 10))
        pixmap = QPixmap('./buttons/pga.png')
        self.label_1.setPixmap(pixmap)
        self.pgaLayout.addWidget(self.label_1)

        self.label_2 = QLabel('pga', self)
        self.label_2.setMaximumSize(QtCore.QSize(60, 30))
        self.label_2.setMinimumSize(QtCore.QSize(60, 30))
        self.label_2.setFont(QFont('Arial', int(screen.width()*0.008)))
        self.pgaLayout.addWidget(self.label_2)

        self.verticalLayout_3.addLayout(self.pgaLayout)

        sizeButton = int(screen.width()*0.02)

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setMaximumSize(QtCore.QSize(sizeButton, sizeButton))
        self.pushButton_3.setMinimumSize(QtCore.QSize(sizeButton, sizeButton))
        self.pushButton_3.setStyleSheet("QPushButton{\n"
                                        "    image: url(./buttons/undo.png);\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:pressed{\n"
                                        "    image: url(./buttons/undoPressed.png);\n"
                                        "}\n"
                                        "")
        self.pushButton_3.setText("")
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.setToolTip('Auto-scale')
        self.verticalLayout_3.addWidget(self.pushButton_3)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setMaximumSize(QtCore.QSize(sizeButton, sizeButton))
        self.pushButton_4.setMinimumSize(QtCore.QSize(sizeButton,sizeButton))
        self.pushButton_4.setStyleSheet("QPushButton{\n"
                                        "    image: url(./buttons/add.png);\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:pressed{\n"
                                        "    image: url(./buttons/addPressed.png);\n"
                                        "}\n"
                                        "")
        self.pushButton_4.setText("")
        self.pushButton_4.setObjectName("pushButton_3")
        self.pushButton_4.setToolTip('Add acceleration data')
        self.verticalLayout_3.addWidget(self.pushButton_4)

        self.gridLayout.addLayout(self.verticalLayout_3, 0, 3, 1, 1)

        spacerItem = QtWidgets.QSpacerItem(20, 40, 0, 0)
        self.gridLayout.addItem(spacerItem, 1, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, 0, 0)
        self.gridLayout.addItem(spacerItem1, 3, 1, 1, 1)

        self.tableWidget_3 = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget_3.setMaximumSize(QtCore.QSize(int(maxTableSize), 16777215))
        self.tableWidget_3.setObjectName("tableWidget_3")
        self.tableWidget_3.setColumnCount(2)
        self.tableWidget_3.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_3.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_3.setHorizontalHeaderItem(1, item)
        self.gridLayout.addWidget(self.tableWidget_3, 0, 0, 1, 1)

        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setMaximumSize(QtCore.QSize(sizeButton, sizeButton))
        self.pushButton.setMinimumSize(QtCore.QSize(sizeButton, sizeButton))
        self.pushButton.setStyleSheet("QPushButton{\n"
                                      "    image: url(./buttons/undo.png);\n"
                                      "}\n"
                                      "\n"
                                      "QPushButton:pressed{\n"
                                      "    image: url(./buttons/undoPressed.png);\n"
                                      "}\n"
                                      "")
        self.pushButton.setText("")
        self.pushButton.setObjectName("pushButton")
        self.pushButton.setToolTip('Auto-scale')
        self.verticalLayout_6.addWidget(self.pushButton)
        self.gridLayout.addLayout(self.verticalLayout_6, 2, 3, 1, 1)

        self.tableWidget_2 = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget_2.setMaximumSize(QtCore.QSize(int(maxTableSize), 16777215))
        self.tableWidget_2.setObjectName("tableWidget_2")
        self.tableWidget_2.setColumnCount(2)
        self.tableWidget_2.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_2.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_2.setHorizontalHeaderItem(1, item)
        self.gridLayout.addWidget(self.tableWidget_2, 2, 0, 1, 1)
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setMaximumSize(QtCore.QSize(int(maxTableSize), 16777215))
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        self.gridLayout.addWidget(self.tableWidget, 4, 0, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout)

        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setMaximumSize(QtCore.QSize(sizeButton, sizeButton))
        self.pushButton_2.setMinimumSize(QtCore.QSize(sizeButton, sizeButton))
        self.pushButton_2.setStyleSheet("QPushButton{\n"
                                        "    image: url(./buttons/undo.png);\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:pressed{\n"
                                        "    image: url(./buttons/undoPressed.png);\n"
                                        "}\n"
                                        "")
        self.pushButton_2.setText("")
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.setToolTip('Auto-scale')
        self.verticalLayout_7.addWidget(self.pushButton_2)
        self.gridLayout.addLayout(self.verticalLayout_7, 4, 3, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1047, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuTools = QtWidgets.QMenu(self.menubar)
        self.menuTools.setObjectName("menuTools")
        self.menuLangue = QtWidgets.QMenu(self.menubar)
        self.menuLangue.setObjectName("menuLangue")
        MainWindow.setMenuBar(self.menubar)
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.actionEnglish = QtWidgets.QAction(MainWindow)
        self.actionEnglish.setObjectName("actionEnglish")
        self.actionFrancais = QtWidgets.QAction(MainWindow)
        self.actionFrancais.setObjectName("actionFrancais")
        self.menuFile.addAction(self.actionOpen)
        self.menuLangue.addAction(self.actionEnglish)
        self.menuLangue.addAction(self.actionFrancais)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuTools.menuAction())
        self.menubar.addAction(self.menuLangue.menuAction())

        self.proxy1 = pg.SignalProxy(self.graphicsView.scene().sigMouseMoved, rateLimit=60,
                                     slot=self.mouseMovedAcc)
        self.proxy2 = pg.SignalProxy(self.graphicsView_2.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMovedVel)
        self.proxy3 = pg.SignalProxy(self.graphicsView_3.scene().sigMouseMoved, rateLimit=60,
                                     slot=self.mouseMovedDis)

        self.cursorAcc = self.graphicsView.plot([0], [0], pen=self.pen, symbol='o', symbolSize=15, symbolBrush=(125, 125, 125),
                                        width=3)
        self.cursorVel = self.graphicsView_2.plot([0], [0], pen=self.pen, symbol='o', symbolSize=15,
                                                symbolBrush=(125, 125, 125),
                                                width=3)
        self.cursorDis = self.graphicsView_3.plot([0], [0], pen=self.pen, symbol='o', symbolSize=15,
                                                symbolBrush=(125, 125, 125),
                                                width=3)
        self.textAcc = pg.TextItem(
            str(str(format(0, '.4f')) + ' (s)\n' + str(format(0, '.4f')) + ' (m/s2)'))
        self.graphicsView.addItem(self.textAcc, ignoreBounds=True)
        self.textAcc.setPos(0, 0)
        self.textAcc.setColor(color=(0, 0, 0))

        self.textVel = pg.TextItem(
            str(str(format(0, '.4f')) + ' (s)\n' + str(format(0, '.4f')) + ' (m/s)'))
        self.graphicsView_2.addItem(self.textVel, ignoreBounds=True)
        self.textVel.setPos(0, 0)
        self.textVel.setColor(color=(0, 0, 0))

        self.textDis = pg.TextItem(
            str(str(format(0, '.4f')) + ' (s)\n' + str(format(0, '.4f')) + ' (m)'))
        self.graphicsView_3.addItem(self.textDis, ignoreBounds=True)
        self.textDis.setPos(0, 0)
        self.textDis.setColor(color=(0, 0, 0))


        self.retranslateUi(MainWindow)
        self.connect_button(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.vbAcc = self.graphicsView.getViewBox()
        self.vbAcc.sigXRangeChanged.connect(self.setYRangeAcc)

        self.vbVel = self.graphicsView_2.getViewBox()
        self.vbVel.sigXRangeChanged.connect(self.setYRangeVel)

        self.vbDis = self.graphicsView_3.getViewBox()
        self.vbDis.sigXRangeChanged.connect(self.setYRangeDis)

    def setYRangeAcc(self):
        self.vbAcc.enableAutoRange(axis='y')
        self.vbAcc.setAutoVisible(y=True)

    def setYRangeVel(self):
        self.vbVel.enableAutoRange(axis='y')
        self.vbVel.setAutoVisible(y=True)

    def setYRangeDis(self):
        self.vbDis.enableAutoRange(axis='y')
        self.vbDis.setAutoVisible(y=True)

    ## Open a dialog window to choose the data file and to choose witch data it correspond
    def open_dialog_box(self, MainWindow):
        filename = QFileDialog.getOpenFileName()

        ## Acceleration
        data = document.read(filename[0])
        self.ui = dialogCheck.Ui_Dialog(data, self.dialogSize)
        self.setUpGraphics('Acceleration')

        ## Velocity
        fileVel = ''
        i = 0
        while i < len(filename[0])- 3:
            fileVel += filename[0][i]
            i+=1
        fileVel += 'vel'
        print(fileVel)
        data = document.read(fileVel)
        self.extractData(data, self.speedX, self.speedY)
        self.setUpGraphics('Vitesse')

        ## Displacement
        fileDis = ''
        i = 0
        while i < len(filename[0]) - 3:
            fileDis += filename[0][i]
            i += 1
        fileDis += 'dis'
        print(fileDis)
        data = document.read(fileDis)
        self.extractData(data, self.mouvementX, self.mouvementY)
        self.setUpGraphics('Deplacement')



    def extractData(self, data, listX, listY):
        i = 0
        while i < len(data):
            listX.append(data[i])
            listY.append(data[i + 1])
            i += 2


    def getMax(self, list):
        index=0
        max = list[0]
        while index < len(list):
            if max < list[index]:
                max = list[index]
            index+=1
        return max + 0.01

    def getMin(self, list):
        index=0
        min = list[0]
        while index < len(list):
            if min > list[index]:
                min = list[index]
            index+=1
        return min - 0.01

    ## set the data in the correct graphic view
    def setUpGraphics(self, type):
        if type == '':
            print('null')
        if type == 'Acceleration':
            print('Acceleration')
            self.graphicsView.clear()
            self.accelerationX = self.ui.dataX
            self.accelerationY = self.ui.dataY
            self.pen = pg.mkPen(color=(255, 0, 0), width=3)
            self.graphicsView.plot(self.accelerationX, self.accelerationY, pen=self.pen)
            pga = self.findMaxValuePG(self.accelerationY)
            self.graphicsView.plot([self.accelerationX[pga]], [self.accelerationY[pga]], pen=self.pen, symbol='o', symbolSize=15,
                                                    symbolBrush=(255, 0, 0), width=3)

            self.textPga = pg.TextItem(
                str(str(format(self.accelerationX[pga], '.4f')) + ' (s)\n' + str(format(self.accelerationY[pga], '.4f')) + ' (m/s2)'))
            self.graphicsView.addItem(self.textPga, ignoreBounds=True)
            self.textPga.setPos(self.accelerationX[pga], self.accelerationY[pga])
            self.textPga.setColor(color=(0, 0, 0))

            self.graphicsView.getViewBox().setLimits(xMin=self.accelerationX[0],
                                                     xMax=self.accelerationX[len(self.accelerationX) - 1],
                                                     yMin=self.getMin(self.accelerationY),
                                                     yMax=self.getMax(self.accelerationY))
            self.showData(self.accelerationX, self.accelerationY, self.tableWidget_3)
        if type == 'Vitesse':
            print('Vitesse')
            self.graphicsView_2.clear()
            self.pen = pg.mkPen(color=(0, 255, 0), width=3)
            self.graphicsView_2.plot(self.speedX, self.speedY, pen=self.pen)
            pgv = self.findMaxValuePG(self.speedY)
            self.graphicsView_2.plot([self.speedX[pgv]], [self.speedY[pgv]], pen=self.pen,
                                              symbol='o', symbolSize=15,
                                              symbolBrush=(255, 0, 0), width=3)
            self.textPgv = pg.TextItem(
                str(str(format(self.speedX[pgv], '.4f')) + ' (s)\n' + str(
                    format(self.speedY[pgv], '.4f')) + ' (m/s2)'))
            self.graphicsView_2.addItem(self.textPgv, ignoreBounds=True)
            self.textPgv.setPos(self.speedX[pgv], self.speedY[pgv])
            self.textPgv.setColor(color=(0, 0, 0))

            self.graphicsView_2.getViewBox().setLimits(xMin=self.speedX[0],
                                                     xMax=self.speedX[len(self.speedX) - 1],
                                                     yMin=self.getMin(self.speedY),
                                                     yMax=self.getMax(self.speedY))
            self.showData(self.speedX, self.speedY, self.tableWidget_2)
        if type == 'Deplacement':
            print('Deplacement')
            self.graphicsView_3.clear()
            self.pen = pg.mkPen(color=(0, 0, 255), width=3)
            self.graphicsView_3.plot(self.mouvementX, self.mouvementY, pen=self.pen)
            pgd = self.findMaxValuePG(self.mouvementY)
            self.graphicsView_3.plot([self.mouvementX[pgd]], [self.mouvementY[pgd]], pen=self.pen,
                                     symbol='o', symbolSize=15,
                                     symbolBrush=(255, 0, 0), width=3)

            self.textPgd = pg.TextItem(
                str(str(format(self.mouvementX[pgd], '.4f')) + ' (s)\n' + str(
                    format(self.mouvementY[pgd], '.4f')) + ' (m/s2)'))
            self.graphicsView_2.addItem(self.textPgd, ignoreBounds=True)
            self.textPgd.setPos(self.mouvementX[pgd], self.mouvementY[pgd])
            self.textPgd.setColor(color=(0, 0, 0))

            self.graphicsView_3.getViewBox().setLimits(xMin=self.mouvementX[0],
                                                     xMax=self.mouvementX[len(self.mouvementX) - 1],
                                                     yMin=self.getMin(self.mouvementY),
                                                     yMax=self.getMax(self.mouvementY))
            self.showData(self.mouvementX, self.mouvementY, self.tableWidget)

    def showData(self, listX, listY, tableWidget):
        tableWidget.setRowCount(len(listX))
        index = 0
        while index < len(listX):
            tableWidget.setItem(index, 0, QtWidgets.QTableWidgetItem(str(listX[index])))
            tableWidget.setItem(index, 1, QtWidgets.QTableWidgetItem(str(listY[index])))
            index += 1





        ## Find the closest point under the mouse in the graphic view and return is index inside the array

    def findClosePoint(self, x, listX):
        i = 1;
        temp = math.fabs(listX[0] - x)
        exit = 0
        while i < len(listX):
            dx = math.fabs(listX[i] - x)
            if dx < temp:
                temp = dx
                exit = i
            else:
                i =  len(listX)
            i += 1
        return exit

    ## You can change the language here
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate

        MainWindow.setWindowTitle(_translate("MainWindow", "ScaleQwake"))
        item = self.tableWidget_3.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Time (s)"))
        item = self.tableWidget_3.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Acceleration (m/s2)"))
        item = self.tableWidget_2.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Time (s)"))
        item = self.tableWidget_2.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Velovity (m/s)"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Time (s)"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Displacement (m)"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuTools.setTitle(_translate("MainWindow", "Tools"))
        self.menuLangue.setTitle(_translate("MainWindow", "Language"))
        self.actionOpen.setText(_translate("MainWindow", "Enter acceleration data"))
        self.actionEnglish.setText(_translate("MainWindow", "English"))
        self.actionFrancais.setText(_translate("MainWindow", "Francais"))

    ## connect all the slots and signals
    def connect_button(self, MainWindow):
        self.actionOpen.triggered.connect(self.open_dialog_box)
        self.pushButton_3.clicked.connect(lambda: self.graphicsView.getPlotItem().enableAutoRange())
        self.pushButton_2.clicked.connect(lambda: self.graphicsView_3.getPlotItem().enableAutoRange())
        self.pushButton.clicked.connect(lambda: self.graphicsView_2.getPlotItem().enableAutoRange())

        self.pushButton_4.clicked.connect(self.open_dialog_box)

        self.tableWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.tableWidget_3.viewport().installEventFilter(self)
        self.tableWidget_2.viewport().installEventFilter(self)
        self.tableWidget.viewport().installEventFilter(self)

        self.graphicsView.viewport().installEventFilter(self)

    def eventFilter(self, source, event):
        ##print(event)
        if (event.type() == QtCore.QEvent.MouseButtonPress and
                event.buttons() == QtCore.Qt.RightButton and
                (source is self.tableWidget_3.viewport() or
                    source is self.tableWidget_2.viewport() or
                    source is self.tableWidget.viewport())):
            item = self.tableWidget_3.itemAt(event.pos())
            print('Global Pos:', event.globalPos())
            if item is not None:
                print('Table Item:', item.row(), item.column())
                self.menu = QMenu(self)
                copie = self.menu.addAction('Copie')  # (QAction('test'))
                action = self.menu.exec_(event.globalPos())
                if action == copie:
                    self.copieAction()

        if event.type() == QtCore.QEvent.Wheel:
            self.setZoom()
        return super(Ui_MainWindow, self).eventFilter(source, event)

    def setZoom(self):
        print('Zoom')
        self.graphicsView.scale(1000,1000)

    def copieAction(self):
        self.copied_cells = sorted(
            self.tableWidget_3.selectedIndexes() or
            self.tableWidget_2.selectedIndexes() or
            self.tableWidget.selectedIndexes())
        if self.copied_cells:
            rows = sorted(index.row() for index in self.copied_cells)
            columns = sorted(index.column() for index in self.copied_cells)
            rowcount = rows[-1] - rows[0] + 1
            colcount = columns[-1] - columns[0] + 1
            table = [[''] * colcount for _ in range(rowcount)]
            for index in self.copied_cells:
                row = index.row() - rows[0]
                column = index.column() - columns[0]
                table[row][column] = index.data()
            stream = io.StringIO()
            csv.writer(stream, delimiter='\t').writerows(table)
            QtWidgets.qApp.clipboard().setText(stream.getvalue())

    def keyPressEvent(self, event):
        super().keyPressEvent(event)
        print('keyPress')
        if event.key() == Qt.Key_C and (event.modifiers() & Qt.ControlModifier):
            self.copieAction()

    def findMaxValuePG(self, list):
        index = 0
        maxIndex = None
        max = math.fabs(list[0])
        while index < len(list):
            if max < math.fabs(list[index]):
                max = math.fabs(list[index])
                maxIndex = index
            index += 1
        return maxIndex


    def showCursorAcc(self, positionX, listX, listY):
        point = self.findClosePoint(positionX, listX)
        x = listX[point]
        y = listY[point]
        self.graphicsView.removeItem(self.cursorAcc)
        self.graphicsView.removeItem(self.textAcc)
        self.cursorAcc = self.graphicsView.plot([x], [y], pen=self.pen, symbol='o', symbolSize=15, symbolBrush=(125, 125, 125), width=3)
        self.textAcc = pg.TextItem(
            str(str(format(x, '.4f')) + ' (s)\n' + str(format(y, '.4f')) + ' (m/s2)'))
        self.graphicsView.addItem(self.textAcc, ignoreBounds=True)
        self.textAcc.setPos(x, y)
        self.textAcc.setColor(color=(0, 0, 0))

    def showCursorVel(self, positionX, listX, listY):
        point = self.findClosePoint(positionX, listX)
        x = listX[point]
        y = listY[point]
        self.graphicsView_2.removeItem(self.cursorVel)
        self.graphicsView_2.removeItem(self.textVel)
        self.cursorVel = self.graphicsView_2.plot([x], [y], pen=self.pen, symbol='o', symbolSize=15, symbolBrush=(125, 125, 125), width=3)
        self.textVel = pg.TextItem(
            str(str(format(x, '.4f')) + ' (s)\n' + str(format(y, '.4f')) + ' (m/s)'))
        self.graphicsView_2.addItem(self.textVel, ignoreBounds=True)
        self.textVel.setPos(x, y)
        self.textVel.setColor(color=(0, 0, 0))

    def showCursorDis(self, positionX, listX, listY):
        point = self.findClosePoint(positionX, listX)
        x = listX[point]
        y = listY[point]
        self.graphicsView_3.removeItem(self.cursorDis)
        self.graphicsView_3.removeItem(self.textDis)
        self.cursorDis = self.graphicsView_3.plot([x], [y], pen=self.pen, symbol='o', symbolSize=15, symbolBrush=(125, 125, 125), width=3)
        self.textDis = pg.TextItem(
            str(str(format(x, '.4f')) + ' (s)\n' + str(format(y, '.4f')) + ' (m)'))
        self.graphicsView_3.addItem(self.textDis, ignoreBounds=True)
        self.textDis.setPos(x, y)
        self.textDis.setColor(color=(0, 0, 0))

    def mouseMovedAcc(self, e):
        pos = e[0]
        positionX = self.graphicsView.plotItem.vb.mapSceneToView(pos).x()
        if len(self.accelerationX) > 0 and len(self.accelerationY) > 0:
            self.pen = pg.mkPen(color=(255, 0, 0), width=3)
            self.showCursorAcc(positionX, self.accelerationX, self.accelerationY)
            self.pen = pg.mkPen(color=(0, 255, 0), width=3)
            self.showCursorVel(positionX, self.speedX, self.speedY)
            self.pen = pg.mkPen(color=(0, 0, 255), width=3)
            self.showCursorDis(positionX, self.mouvementX, self.mouvementY)

    def mouseMovedVel(self, e):
        pos = e[0]
        positionX = self.graphicsView_2.plotItem.vb.mapSceneToView(pos).x()
        if len(self.accelerationX) > 0 and len(self.accelerationY) > 0:
            self.pen = pg.mkPen(color=(255, 0, 0), width=3)
            self.showCursorAcc(positionX, self.accelerationX, self.accelerationY)
            self.pen = pg.mkPen(color=(0, 255, 0), width=3)
            self.showCursorVel(positionX, self.speedX, self.speedY)
            self.pen = pg.mkPen(color=(0, 0, 255), width=3)
            self.showCursorDis(positionX, self.mouvementX, self.mouvementY)

    def mouseMovedDis(self, e):
        pos = e[0]
        positionX = self.graphicsView_3.plotItem.vb.mapSceneToView(pos).x()
        if len(self.accelerationX) > 0 and len(self.accelerationY) > 0:
            self.pen = pg.mkPen(color=(255, 0, 0), width=3)
            self.showCursorAcc(positionX, self.accelerationX, self.accelerationY)
            self.pen = pg.mkPen(color=(0, 255, 0), width=3)
            self.showCursorVel(positionX, self.speedX, self.speedY)
            self.pen = pg.mkPen(color=(0, 0, 255), width=3)
            self.showCursorDis(positionX, self.mouvementX, self.mouvementY)






if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
